#!/bin/bash

if [ ! -d log ]
then
	mkdir log
fi
cd log

read -p 'Which files to create the statistics? ' FILE

echo 'Scenario,Bin packing,Mu,Turn off,Postpone,All,LR'
for f in $FILE
do
	echo -n $f,`sed -n 2p $f | cut -f1 -d,`,	# Initial
	echo -n `tail -1 ${f}m | cut -f2 -d,`,		# Mu
	echo -n `tail -1 ${f}t | cut -f2 -d,`,		# Turn off
	echo -n `tail -1 ${f}p | cut -f2 -d,`,		# Postpone
	echo -n `tail -1 $f | cut -f2 -d,`,			# Primal
	echo `tail -1 $f | cut -f3 -d,`				# LR
done

exit

echo 'Scenario,Bin packing,Mu,Turn off,Postpone,All,LR'
for suf in u b
do
	for qnt in `seq 200 100 800`
	do
		echo -n $qnt$suf,`sed -n 3p $qnt$suf.log | cut -f1 -d,`,	# Initial
		echo -n `tail -1 $qnt${suf}m.log | cut -f2 -d,`,			# Mu
		echo -n `tail -1 $qnt${suf}t.log | cut -f2 -d,`,			# Turn off
		echo -n `tail -1 $qnt${suf}p.log | cut -f2 -d,`,			# Postpone
		echo -n `tail -1 $qnt${suf}.log | cut -f2 -d,`,				# All
		echo `tail -1 $qnt$suf.log | cut -f3 -d,`					# LR
	done
done

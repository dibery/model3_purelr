debug:
	g++ main.cpp -std=c++11 -o main.exe -g -Wall -Wextra -ftrapv -DBURST_TASK
uniform:
	g++ main.cpp -std=c++11 -o main.exe -O3 -s -w -ftrapv
burst:
	g++ main.cpp -std=c++11 -o main.exe -O3 -s -w -ftrapv -DBURST_TASK
clean:
	rm -rf *.o main.exe obj bin
show:
	g++ main.cpp -std=c++11 -o main.exe -O3 -s -w -ftrapv -DBURST_TASK -DSHOW
showx:
	g++ main.cpp -std=c++11 -o main.exe -O3 -s -w -ftrapv -DBURST_TASK -DSHOW_X

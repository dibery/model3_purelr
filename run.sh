# All
# Uniform cases
./umain -task 200 > log/ut200
./umain -task 300 > log/ut300
./umain -task 400 > log/ut400
./umain -task 500 > log/ut500
./umain -task 600 > log/ut600
./umain -task 700 > log/ut700
./umain -task 800 > log/ut800
# Different task numbers
./main -task 200 > log/t200
./main -task 300 > log/t300
./main -task 400 > log/t400
./main -task 500 > log/t500
./main -task 600 > log/t600
./main -task 700 > log/t700
./main -task 800 > log/t800
# High capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 800 -memory 800 > log/h2
./main -task 400 -type 1 -server 4 -cpu 400 -memory 400 > log/h4
./main -task 400 -type 1 -server 6 -cpu 267 -memory 267 > log/h6
./main -task 400 -type 1 -server 8 -cpu 200 -memory 200 > log/h8
./main -task 400 -type 1 -server 10 -cpu 160 -memory 160 > log/h10
# Middle capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 500 -memory 500 > log/m2
./main -task 400 -type 1 -server 4 -cpu 250 -memory 250 > log/m4
./main -task 400 -type 1 -server 6 -cpu 167 -memory 167 > log/m6
./main -task 400 -type 1 -server 8 -cpu 125 -memory 125 > log/m8
./main -task 400 -type 1 -server 10 -cpu 100 -memory 100 > log/m10
# Low capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 250 -memory 250 > log/l2
./main -task 400 -type 1 -server 4 -cpu 125 -memory 125 > log/l4
./main -task 400 -type 1 -server 6 -cpu 83 -memory 83 > log/l6
./main -task 400 -type 1 -server 8 -cpu 62 -memory 62 > log/l8
./main -task 400 -type 1 -server 10 -cpu 50 -memory 50 > log/l10
# Different process time
./main -process 1 > log/p1
./main -process 2 > log/p2
./main -process 4 > log/p4
./main -process 8 > log/p8
./main -process 16 > log/p16
./main -process 32 > log/p32
./main -process 64 > log/p64
# Different wait time
./main -wait 0 > log/w0
./main -wait 2 > log/w2
./main -wait 5 > log/w5
./main -wait 10 > log/w10
./main -wait 20 > log/w20
./main -wait 40 > log/w40
./main -wait 80 > log/w80
# Mu
# Uniform cases
./umain -task 200 > log/ut200m -mode 1
./umain -task 300 > log/ut300m -mode 1
./umain -task 400 > log/ut400m -mode 1
./umain -task 500 > log/ut500m -mode 1
./umain -task 600 > log/ut600m -mode 1
./umain -task 700 > log/ut700m -mode 1
./umain -task 800 > log/ut800m -mode 1
# Different task numbers
./main -task 200 > log/t200m -mode 1
./main -task 300 > log/t300m -mode 1
./main -task 400 > log/t400m -mode 1
./main -task 500 > log/t500m -mode 1
./main -task 600 > log/t600m -mode 1
./main -task 700 > log/t700m -mode 1
./main -task 800 > log/t800m -mode 1
# High capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 800 -memory 800 > log/h2m -mode 1
./main -task 400 -type 1 -server 4 -cpu 400 -memory 400 > log/h4m -mode 1
./main -task 400 -type 1 -server 6 -cpu 267 -memory 267 > log/h6m -mode 1
./main -task 400 -type 1 -server 8 -cpu 200 -memory 200 > log/h8m -mode 1
./main -task 400 -type 1 -server 10 -cpu 160 -memory 160 > log/h10m -mode 1
# Middle capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 500 -memory 500 > log/m2m -mode 1
./main -task 400 -type 1 -server 4 -cpu 250 -memory 250 > log/m4m -mode 1
./main -task 400 -type 1 -server 6 -cpu 167 -memory 167 > log/m6m -mode 1
./main -task 400 -type 1 -server 8 -cpu 125 -memory 125 > log/m8m -mode 1
./main -task 400 -type 1 -server 10 -cpu 100 -memory 100 > log/m10m -mode 1
# Low capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 250 -memory 250 > log/l2m -mode 1
./main -task 400 -type 1 -server 4 -cpu 125 -memory 125 > log/l4m -mode 1
./main -task 400 -type 1 -server 6 -cpu 83 -memory 83 > log/l6m -mode 1
./main -task 400 -type 1 -server 8 -cpu 62 -memory 62 > log/l8m -mode 1
./main -task 400 -type 1 -server 10 -cpu 50 -memory 50 > log/l10m -mode 1
# Different process time
./main -process 1 > log/p1m -mode 1
./main -process 2 > log/p2m -mode 1
./main -process 4 > log/p4m -mode 1
./main -process 8 > log/p8m -mode 1
./main -process 16 > log/p16m -mode 1
./main -process 32 > log/p32m -mode 1
./main -process 64 > log/p64m -mode 1
# Different wait time
./main -wait 0 > log/w0m -mode 1
./main -wait 2 > log/w2m -mode 1
./main -wait 5 > log/w5m -mode 1
./main -wait 10 > log/w10m -mode 1
./main -wait 20 > log/w20m -mode 1
./main -wait 40 > log/w40m -mode 1
./main -wait 80 > log/w80m -mode 1
# Turn off
# Uniform cases
./umain -task 200 > log/ut200t -mode 2
./umain -task 300 > log/ut300t -mode 2
./umain -task 400 > log/ut400t -mode 2
./umain -task 500 > log/ut500t -mode 2
./umain -task 600 > log/ut600t -mode 2
./umain -task 700 > log/ut700t -mode 2
./umain -task 800 > log/ut800t -mode 2
# Different task numbers
./main -task 200 > log/t200t -mode 2
./main -task 300 > log/t300t -mode 2
./main -task 400 > log/t400t -mode 2
./main -task 500 > log/t500t -mode 2
./main -task 600 > log/t600t -mode 2
./main -task 700 > log/t700t -mode 2
./main -task 800 > log/t800t -mode 2
# High capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 800 -memory 800 > log/h2t -mode 2
./main -task 400 -type 1 -server 4 -cpu 400 -memory 400 > log/h4t -mode 2
./main -task 400 -type 1 -server 6 -cpu 267 -memory 267 > log/h6t -mode 2
./main -task 400 -type 1 -server 8 -cpu 200 -memory 200 > log/h8t -mode 2
./main -task 400 -type 1 -server 10 -cpu 160 -memory 160 > log/h10t -mode 2
# Middle capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 500 -memory 500 > log/m2t -mode 2
./main -task 400 -type 1 -server 4 -cpu 250 -memory 250 > log/m4t -mode 2
./main -task 400 -type 1 -server 6 -cpu 167 -memory 167 > log/m6t -mode 2
./main -task 400 -type 1 -server 8 -cpu 125 -memory 125 > log/m8t -mode 2
./main -task 400 -type 1 -server 10 -cpu 100 -memory 100 > log/m10t -mode 2
# Low capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 250 -memory 250 > log/l2t -mode 2
./main -task 400 -type 1 -server 4 -cpu 125 -memory 125 > log/l4t -mode 2
./main -task 400 -type 1 -server 6 -cpu 83 -memory 83 > log/l6t -mode 2
./main -task 400 -type 1 -server 8 -cpu 62 -memory 62 > log/l8t -mode 2
./main -task 400 -type 1 -server 10 -cpu 50 -memory 50 > log/l10t -mode 2
# Different process time
./main -process 1 > log/p1t -mode 2
./main -process 2 > log/p2t -mode 2
./main -process 4 > log/p4t -mode 2
./main -process 8 > log/p8t -mode 2
./main -process 16 > log/p16t -mode 2
./main -process 32 > log/p32t -mode 2
./main -process 64 > log/p64t -mode 2
# Different wait time
./main -wait 0 > log/w0t -mode 2
./main -wait 2 > log/w2t -mode 2
./main -wait 5 > log/w5t -mode 2
./main -wait 10 > log/w10t -mode 2
./main -wait 20 > log/w20t -mode 2
./main -wait 40 > log/w40t -mode 2
./main -wait 80 > log/w80t -mode 2
# Postpone
# Uniform cases
./umain -task 200 > log/ut200p -mode 4
./umain -task 300 > log/ut300p -mode 4
./umain -task 400 > log/ut400p -mode 4
./umain -task 500 > log/ut500p -mode 4
./umain -task 600 > log/ut600p -mode 4
./umain -task 700 > log/ut700p -mode 4
./umain -task 800 > log/ut800p -mode 4
# Different task numbers
./main -task 200 > log/t200p -mode 4
./main -task 300 > log/t300p -mode 4
./main -task 400 > log/t400p -mode 4
./main -task 500 > log/t500p -mode 4
./main -task 600 > log/t600p -mode 4
./main -task 700 > log/t700p -mode 4
./main -task 800 > log/t800p -mode 4
# High capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 800 -memory 800 > log/h2p -mode 4
./main -task 400 -type 1 -server 4 -cpu 400 -memory 400 > log/h4p -mode 4
./main -task 400 -type 1 -server 6 -cpu 267 -memory 267 > log/h6p -mode 4
./main -task 400 -type 1 -server 8 -cpu 200 -memory 200 > log/h8p -mode 4
./main -task 400 -type 1 -server 10 -cpu 160 -memory 160 > log/h10p -mode 4
# Middle capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 500 -memory 500 > log/m2p -mode 4
./main -task 400 -type 1 -server 4 -cpu 250 -memory 250 > log/m4p -mode 4
./main -task 400 -type 1 -server 6 -cpu 167 -memory 167 > log/m6p -mode 4
./main -task 400 -type 1 -server 8 -cpu 125 -memory 125 > log/m8p -mode 4
./main -task 400 -type 1 -server 10 -cpu 100 -memory 100 > log/m10p -mode 4
# Low capacity, different server#
./main -task 400 -type 1 -server 2 -cpu 250 -memory 250 > log/l2p -mode 4
./main -task 400 -type 1 -server 4 -cpu 125 -memory 125 > log/l4p -mode 4
./main -task 400 -type 1 -server 6 -cpu 83 -memory 83 > log/l6p -mode 4
./main -task 400 -type 1 -server 8 -cpu 62 -memory 62 > log/l8p -mode 4
./main -task 400 -type 1 -server 10 -cpu 50 -memory 50 > log/l10p -mode 4
# Different process time
./main -process 1 > log/p1p -mode 4
./main -process 2 > log/p2p -mode 4
./main -process 4 > log/p4p -mode 4
./main -process 8 > log/p8p -mode 4
./main -process 16 > log/p16p -mode 4
./main -process 32 > log/p32p -mode 4
./main -process 64 > log/p64p -mode 4
# Different wait time
./main -wait 0 > log/w0p -mode 4
./main -wait 2 > log/w2p -mode 4
./main -wait 5 > log/w5p -mode 4
./main -wait 10 > log/w10p -mode 4
./main -wait 20 > log/w20p -mode 4
./main -wait 40 > log/w40p -mode 4
./main -wait 80 > log/w80p -mode 4

#include <bits/stdc++.h>
//#define SHOW
//#define SUB_SHOW
//#define BURST_TASK
//#define SHOW_X
using namespace std;

int SEED = 0;
double lambda = 2, ITER_LIMIT = 8000, IMPROVE_LIMIT = 500;
const int MAX_TIME = 100, MAX_TASK = 800, MAX_SERVER = 10, MAX_TYPES = 2; // 最大的可能數值，只用於宣告陣列之用
int TIME = 100, TASK = 400, SERVER = 4, TYPES = 2, BATCH = 5;

int BASE_CPU = 250, BASE_MEMORY = 250;
double BASE_COST_RATE = .2; // 計算基礎開機成本
const double RATE = 0.5, COST_RATE = 1.5; // 等級提升一級所產生的倍率

int OPEN = 1, OMEGA = 10;
const int MU = 1 << 0, TURN_OFF = 1 << 1, POSTPONE = 1 << 2;
int PRIMAL_MODE = MU | TURN_OFF | POSTPONE;

double z_star, zd = INT_MIN, iter, improve_ctr, LB = INT_MIN;

double pa[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ], px[ MAX_TIME ][ MAX_SERVER ], py[ MAX_TIME ][ MAX_SERVER ], pb[ MAX_TASK ], pu[ MAX_TASK ][ MAX_SERVER ]; // primal solutions

FILE *f = fopen( "rand.txt", "r" );

int my_rand()
{
	int x;
	if( fscanf( f, "%d", &x ) == EOF )
		fprintf( stderr, "Random file reached EOF\n" );
	return x;
}

#include "task.cpp"
#include "server.cpp"

server Server[ MAX_SERVER ];
task Task[ MAX_TASK ];

/** functions **/
double sol_sub1(), sol_sub2(), sol_sub3(), sol_sub4(), sol_sub5();
double primal(), init_sol();
void LR(), adjust_multiplier(), init( int, char*[] );

int main( int argc, char* argv[] )
{
	init( argc, argv );
	init_sol();
	cout << z_star << endl;
}

void init( int argc, char* argv[] )
{
	srand( SEED );

	/** Initialize parameters from cli **/

	for( int i = 1; i < argc; i += 2 )
		if( !strcmp( argv[ i ], "-server" ) )
			SERVER = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-task" ) )
			TASK = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-time" ) )
			TIME = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-memory" ) )
			BASE_MEMORY = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-cpu" ) )
			BASE_CPU = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-process" ) )
			task::BASE_GAMMA = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-wait" ) )
			task::waiting_time = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-penalty" ) )
			task::PENALTY = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-mode" ) )
			PRIMAL_MODE = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-type" ) )
			TYPES = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-value" ) )
			task::MAX_V = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-basic_cost" ) )
			BASE_COST_RATE = strtod( argv[ i + 1 ], NULL );
		else
			fprintf( stderr, "Wrong option %s!\n", argv[ i ] );

	/** Initialize servers **/

	vector<int> level;
	for( int s = 0; s < SERVER; ++s )
		level.push_back( s / ( SERVER / TYPES ) );
	sort( level.begin(), level.end() );
	for( int i = 0; i < SERVER; ++i )
	{
		Server[ i ].set_lv( level[ i ] );
		Server[ i ].link( Task );
	}

	/** Initialize tasks **/

#ifdef BURST_TASK
	task::init_re();
#endif // BURST_TASK
	for( auto& t: Task )
		t.init();
}

double init_sol()
{
	memset( pa, 0, sizeof( pa ) );
	memset( px, 0, sizeof( px ) );
	memset( py, 0, sizeof( py ) );
	memset( pb, 0, sizeof( pb ) );
	z_star = 0;

	vector<int> task_list;
	int residual[ MAX_SERVER ];

	fill( residual, residual + SERVER, INT_MAX );
	for( int i = 0; i < TASK; ++i )
		task_list.push_back( i );
	sort( task_list.begin(), task_list.end(), [] ( int a, int b ) { return Task[ a ].load() > Task[ b ].load(); } );
	for( int i: task_list )
	{
		for( int s = 0; s < SERVER; ++s )
			if( Server[ s ].can_take_init( Task[ i ] ) )
				residual[ s ] = min( Server[ s ].res_cpu( Task[ i ] ), Server[ s ].res_memory( Task[ i ] ) );
		if( find_if( residual, residual + SERVER, [] ( int n ) { return n != INT_MAX; } ) != residual + SERVER )
			Server[ min_element( residual, residual + SERVER ) - residual ].take_init( Task[ i ] );
	}

	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				if( pa[ t ][ i ][ s ] > px[ t ][ s ] )
					px[ t ][ s ] = pa[ t ][ i ][ s ];
	for( int t = 0; t < TIME; ++t )
		if( accumulate( px[ t ], px[ t + 1 ], 0 ) == 0 )
			*px[ t ] = 1;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			if( px[ t ][ s ] && ( !t || !px[ t - 1 ][ s ] ) && any_of( px, px + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
				py[ t ][ s ] = 1;
			else
				py[ t ][ s ] = 0;

	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += pa[ t ][ i ][ s ];
		pb[ i ] = tmp != Task[ i ].process_time; // 不相等就設 1
	}

	// calculate initial z_star
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			z_star += Server[ s ].fix_cost * px[ t ][ s ] + Server[ s ].reopen_cost * py[ t ][ s ];
			for( int i = 0; i < TASK; ++i )
				z_star += -Task[ i ].value[ t ] * pa[ t ][ i ][ s ];
		}
	for( int i = 0; i < TASK; ++i )
		z_star += Task[ i ].penalty * pb[ i ];

	return z_star;
}

struct server
{
	int lv, cpu, memory, fix_cost, reopen_cost, id;
	vector<int> time_slot[ MAX_TASK ], postpone_slot[ MAX_TASK ], drop_list;
	task* task_info[ MAX_TASK ];
	static int ID;
	const static int npos = -1;

	void set_lv( int level )
	{
		cpu = ( 1 + level * RATE ) * BASE_CPU;
		memory = ( 1 + level * RATE ) * BASE_MEMORY;
		fix_cost = ( cpu + memory ) * BASE_COST_RATE;
		reopen_cost = fix_cost * COST_RATE;
		id = ID++;
	}

	void link( task* t )
	{
		for( int i = 0; i < TASK; ++i )
			task_info[ i ] = t + i;
	}

	void clear()
	{
		for( auto& i: postpone_slot )
			i.clear();
		for( auto& i: time_slot )
			i.clear();
	}

	int res_cpu( int t )
	{
		double sum = 0;
		for( int i = 0; i < TASK; ++i )
			if( pa[ t ][ i ][ id ] )
				sum += task_info[ i ]->cpu;
		return cpu - sum;
	}

	int res_cpu( task& t )
	{
		int ans = 0;
		for( int i = t.arrive; i < t.deadline; ++i )
			ans += res_cpu( i );
		return ans;
	}

	int res_memory( task& t )
	{
		int ans = 0;
		for( int i = t.arrive; i < t.deadline; ++i )
			ans += res_memory( i );
		return ans;
	}

	int res_memory( int t )
	{
		double sum = 0;
		for( int i = 0; i < TASK; ++i )
			if( pa[ t ][ i ][ id ] )
				sum += task_info[ i ]->memory;
		return memory - sum;
	}

	bool can_take_init( const task& T )
	{
		int avail = 0;
		for( int i = T.arrive; i < T.deadline; ++i )
			if( res_cpu( i ) >= T.cpu && res_memory( i ) >= T.memory )
				++avail;
		return avail >= T.process_time; // Have enough time for this task
	}

	bool take_init( task& T )
	{
		if( !can_take_init( T ) )
			return false;
		vector<int>& v = time_slot[ T.id ];
		if( !v.empty() )
			v.clear(), puts( "didn't clear!" ); // 基本上這行不會執行到,只是保險起見
		for( int i = T.arrive; i < T.deadline && !T.fulfilled(); ++i )
			if( res_cpu( i ) >= T.cpu && res_memory( i ) >= T.memory )
			{
				pa[ i ][ T.id ][ id ] = 1;
				T.served[ i ] = id;
				v.push_back( i );
			}
		return true;
	}

	bool can_take_primal( task& T )
	{
		/*
		if( rand() % 5 == 0 )
		{
			if( can_take_init( T ) )
			{
				take_init( T );
				return true;
			}
			return false;
		}
		*/
		for( auto& i: postpone_slot )
			i.clear();
		vector<int> priority;
		// 有互相影響的 task
		for( int i = 0; i < TASK; ++i )
			if( !time_slot[ i ].empty() && time_slot[ i ].back() >= T.arrive && time_slot[ i ].front() < T.deadline )
				priority.push_back( i );
		// 決定誰先丟掉 (價值最小在前)
		sort( priority.begin(), priority.end(),
					[ this ] ( const int& a, const int& b )
					{ return task_info[ a ]->total_value() < task_info[ b ]->total_value(); }
				);
		// 把 task 丟掉
		vector<int> put_back;
		for( int dropped_value = 0, i = 0; i < (int)priority.size() && dropped_value <= T.total_value() && !can_take_init( T ); ++i )
		{
			postpone_slot[ priority[ i ] ].swap( time_slot[ priority[ i ] ] );
			drop( *task_info[ priority[ i ] ] );
			put_back.push_back( priority[ i ] );
		}
		if( can_take_init( T ) )
		{
			take_init( T );
			for( int i: put_back ) // 把所有被放在一旁的 task 放回去
				if( !take_init( *task_info[ i ] ) ) // 有任一 task 無法被放回,重置
				{
					drop( T );
					recover( put_back );
					return false;
				}
			return true;
		}
		// T 無法進入系統
		recover( put_back );
		return false;
	}

	void recover( vector<int>& put_back )
	{
		for( int x: put_back )
		{
			drop( *task_info[ x ] );
			time_slot[ x ].swap( postpone_slot[ x ] );
			for( int y: time_slot[ x ] )
				pa[ y ][ x ][ id ] = 1;
			postpone_slot[ x ].clear();
		}
	}

	bool take_primal( task& T )
	{
		return take_init( T ) || ( PRIMAL_MODE & POSTPONE && can_take_primal( T ) );
	}

	void drop( const task& t )
	{
		for( int time: time_slot[ t.id ] )
			pa[ time ][ t.id ][ id ] = 0;
		time_slot[ t.id ].clear();
	}

	void drop( int t )
	{
		drop( *task_info[ t ] );
	}

	void drop_all()
	{
		for( int i = 0; i < TASK; ++i )
			if( !time_slot[ i ].empty() )
				drop( i );
	}

	void show()
	{
		printf( "%d\t%d\t%d\t%d\n", cpu, memory, fix_cost, reopen_cost );
	}

	bool is_on()
	{
		for( int t = 0; t < TIME; ++t )
			if( px[ t ][ id ] )
				return true;
		return false;
	}

	double assoc_rate( double mu2[ MAX_TIME ][ MAX_SERVER ], double mu3[ MAX_TIME ][ MAX_SERVER ] )
	{
		double sum = 0;
		for( int t = 0; t < TIME; ++t )
			sum += mu2[ t ][ id ] + mu3[ t ][ id ];
		if( !is_on() )
			return -1 / sum;
		else
			return sum;
	}

	double total_value()
	{
		double sum = 0;
		set<int> used;
		for( int i = 0; i < TASK; ++i )
			if( !time_slot[ i ].empty() )
			{
				sum += task_info[ i ]->penalty;
				for( int t: time_slot[ i ] )
				{
					sum += task_info[ i ]->value[ t ];
					used.insert( t );
				}
			}
		return sum - used.size() * fix_cost;
	}
};

int server::ID = 0;

#include <bits/stdc++.h>
//#define SHOW
//#define SUB_SHOW
//#define BURST_TASK
//#define SHOW_X
using namespace std;

int SEED = 0;
double lambda = 2, ITER_LIMIT = 8000, IMPROVE_LIMIT = 500;
const int MAX_TIME = 100, MAX_TASK = 800, MAX_SERVER = 10, MAX_TYPES = 2; // 最大的可能數值，只用於宣告陣列之用
int TIME = 100, TASK = 400, SERVER = 4, TYPES = 2, BATCH = 5;

int BASE_CPU = 250, BASE_MEMORY = 250;
double BASE_COST_RATE = .2; // 計算基礎開機成本
const double RATE = 0.5, COST_RATE = 1.5; // 等級提升一級所產生的倍率

int OPEN = 1, OMEGA = 10;
const int MU = 1 << 0, TURN_OFF = 1 << 1, POSTPONE = 1 << 2;
int PRIMAL_MODE = MU | TURN_OFF | POSTPONE;

double z_star, zd = INT_MIN, iter, improve_ctr, LB = INT_MIN;

double mu1[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ], mu2[ MAX_TIME ][ MAX_SERVER ], mu3[ MAX_TIME ][ MAX_SERVER ], mu4[ MAX_TIME ][ MAX_SERVER ], mu5[ MAX_TASK ], mu6[ MAX_TASK ], mu7[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ];
double a[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ], x[ MAX_TIME ][ MAX_SERVER ], y[ MAX_TIME ][ MAX_SERVER ], b[ MAX_TASK ], u[ MAX_TASK ][ MAX_SERVER ]; // sub-problem solutions
double pa[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ], px[ MAX_TIME ][ MAX_SERVER ], py[ MAX_TIME ][ MAX_SERVER ], pb[ MAX_TASK ], pu[ MAX_TASK ][ MAX_SERVER ]; // primal solutions
double ha[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ], hx[ MAX_TIME ][ MAX_SERVER ], hy[ MAX_TIME ][ MAX_SERVER ], hb[ MAX_TASK ]; // heuristic solutions

FILE *f = fopen( "rand.txt", "r" );

int my_rand()
{
	int x;
	if( fscanf( f, "%d", &x ) == EOF )
		fprintf( stderr, "Random file reached EOF\n" );
	return x;
}

#include "task.cpp"
#include "server.cpp"

server Server[ MAX_SERVER ];
task Task[ MAX_TASK ];

/** functions **/
double sol_sub1(), sol_sub2(), sol_sub3(), sol_sub4(), sol_sub5();
double primal(), init_sol();
void LR(), adjust_multiplier(), init( int, char*[] );

int main( int argc, char* argv[] )
{
	init( argc, argv );
	init_sol();
	puts( "UB,Z*,LB" );
	cout << z_star << ',' << z_star << ',' << endl;
	LR();
}

void init( int argc, char* argv[] )
{
	srand( SEED );

	/** Initialize parameters from cli **/

	for( int i = 1; i < argc; i += 2 )
		if( !strcmp( argv[ i ], "-server" ) )
			SERVER = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-task" ) )
			TASK = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-time" ) )
			TIME = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-memory" ) )
			BASE_MEMORY = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-cpu" ) )
			BASE_CPU = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-process" ) )
			task::BASE_GAMMA = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-wait" ) )
			task::waiting_time = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-penalty" ) )
			task::PENALTY = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-mode" ) )
			PRIMAL_MODE = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-type" ) )
			TYPES = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-value" ) )
			task::MAX_V = atoi( argv[ i + 1 ] );
		else if( !strcmp( argv[ i ], "-basic_cost" ) )
			BASE_COST_RATE = strtod( argv[ i + 1 ], NULL );
		else
			fprintf( stderr, "Wrong option %s!\n", argv[ i ] );

	/** Initialize servers **/

	vector<int> level;
	for( int s = 0; s < SERVER; ++s )
		level.push_back( s / ( SERVER / TYPES ) );
	sort( level.begin(), level.end() );
	for( int i = 0; i < SERVER; ++i )
	{
		Server[ i ].set_lv( level[ i ] );
		Server[ i ].link( Task );
	}

#ifdef SHOW
	puts( "Servers:" );
	for( int i = 0; i < SERVER; ++i )
		Server[ i ].show();
#endif // SHOW

	/** Initialize tasks **/

#ifdef BURST_TASK
	task::init_re();
#endif // BURST_TASK
	for( auto& t: Task )
		t.init();
#ifdef SHOW
	// initialized at task declaration, no more code here
	puts( "Tasks:" );
	puts( "ID\tValue\tCPU\tMemory\tArrive\tgamma" );
	for( int i = 0; i < TASK; ++i )
		printf( "%d\t", i ), Task[ i ].show();
#endif // SHOW

	/** Initialize LR variables **/

	memset( mu1, 0, sizeof( mu1 ) );
	memset( mu2, 0, sizeof( mu2 ) );
	memset( mu3, 0, sizeof( mu3 ) );
	memset( mu4, 0, sizeof( mu4 ) );
	memset( mu5, 0, sizeof( mu5 ) );
	memset( mu6, 0, sizeof( mu6 ) );
}

double init_sol()
{
	memset( pa, 0, sizeof( pa ) );
	memset( px, 0, sizeof( px ) );
	memset( py, 0, sizeof( py ) );
	memset( pb, 0, sizeof( pb ) );
	z_star = 0;

	for( int i = 0; i < TASK; ++i )
		for( int s = 0; s < SERVER; ++s )
			if( Server[ s ].take_init( Task[ i ] ) )
				break;

	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				if( pa[ t ][ i ][ s ] > px[ t ][ s ] )
					px[ t ][ s ] = pa[ t ][ i ][ s ];
	for( int t = 0; t < TIME; ++t )
		if( accumulate( px[ t ], px[ t + 1 ], 0 ) == 0 )
			*px[ t ] = 1;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			if( px[ t ][ s ] && ( !t || !px[ t - 1 ][ s ] ) && any_of( px, px + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
				py[ t ][ s ] = 1;
			else
				py[ t ][ s ] = 0;

	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += pa[ t ][ i ][ s ];
		pb[ i ] = tmp != Task[ i ].process_time; // 不相等就設 1
	}

	// calculate initial z_star
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			z_star += Server[ s ].fix_cost * px[ t ][ s ] + Server[ s ].reopen_cost * py[ t ][ s ];
			for( int i = 0; i < TASK; ++i )
				z_star += -Task[ i ].value[ t ] * pa[ t ][ i ][ s ];
		}
	for( int i = 0; i < TASK; ++i )
		z_star += Task[ i ].penalty * pb[ i ];

	return z_star;
}

void LR()
{
	for( int iter = 1; iter <= ITER_LIMIT; ++iter )
	{
		zd = sol_sub1() + sol_sub2() + sol_sub3() + sol_sub4() + sol_sub5();
		double UB = primal();

		// Update bound
		if( zd < LB )
			++improve_ctr;
		else
			LB = zd, improve_ctr = 0;
#ifdef SHOW_X
		if( UB < z_star )
		{
			z_star = UB;
			puts( "Px:" );
			for( int s = 0; s < SERVER; ++s )
				for( int t = 0; t < TIME; ++t )
					printf( "%.0f%c", px[ t ][ s ], t == TIME - 1? '\n' : ' ' );
		}
#else
		z_star = min( z_star, UB );
#endif

		cout << UB << ',' << z_star << ',' << LB << endl;

		if( abs( z_star - LB ) / min( abs( LB ), abs( z_star ) ) < FLT_EPSILON )
		{
#ifdef SHOW
			puts( "gap minimum" );
			printf( "Stop at iteration: %d\n", iter );
#endif // SHOW
			return;
		}
		else if( iter == ITER_LIMIT )
		{
#ifdef SHOW
			puts( "iteration limit reached\n" );
			printf( "Stop at iteration: %d\n", iter );
#endif // SHOW
			return;
		}
		else if( LB > z_star )
		{
#ifdef SHOW
			puts( "LB > z_star" );
			printf( "Stop at iteration: %d\n", iter );
#endif // SHOW
			return;
		}

		if( improve_ctr == IMPROVE_LIMIT )
			lambda /= 2, improve_ctr = 0;
		adjust_multiplier();
	}
}

double sol_sub1()
{
	memset( a, 0, sizeof( a ) );

	for( int i = 0; i < TASK; ++i )
	{
		vector<pair<double,int>> time_slot;
		vector<tuple<double,int,int>> ans;

		for( int t = Task[ i ].arrive; t < Task[ i ].deadline; ++t )
		{
			vector<pair<double,int>> coef;
			for( int s = 0; s < SERVER; ++s )
				coef.push_back( make_pair( -Task[ i ].value[ t ]
								+ mu1[ t ][ i ][ s ]
								+ mu2[ t ][ s ] * Task[ i ].cpu
								+ mu3[ t ][ s ] * Task[ i ].memory
								- mu5[ i ] / Task[ i ].process_time
								+ mu6[ i ]
								+ mu7[ t ][ i ][ s ], s ) );
			pair<double,int> p = *min_element( coef.begin(), coef.end() );
			ans.push_back( make_tuple( p.first, t, p.second ) );
		}
		sort( ans.begin(), ans.end() );
		double tmp = 0;
		for( int n = 0; n < Task[ i ].process_time; ++n )
			tmp += ( -Task[ i ].value[ get<1>( ans[ n ] ) ]
							+ mu1[ get<1>( ans[ n ] ) ][ i ][ get<2>( ans[ n ] ) ]
							+ mu2[ get<1>( ans[ n ] ) ][ get<2>( ans[ n ] ) ] * Task[ i ].cpu
							+ mu3[ get<1>( ans[ n ] ) ][ get<2>( ans[ n ] ) ] * Task[ i ].memory
							- mu5[ i ] / Task[ i ].process_time
							+ mu6[ i ]
							+ mu7[ get<1>( ans[ n ] ) ][ i ][ get<2>( ans[ n ] ) ] );
		if( tmp <= 0 )
			for( int n = 0; n < Task[ i ].process_time; ++n )
				a[ get<1>( ans[ n ] ) ][ i ][ get<2>( ans[ n ] ) ] = 1;
	}

	// Calculate sub-problem result
	double ans = 0;

	for( int i = 0; i < TASK; ++i )
	{
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				ans += ( -Task[ i ].value[ t ]
							+ mu1[ t ][ i ][ s ]
							+ mu2[ t ][ s ] * Task[ i ].cpu
							+ mu3[ t ][ s ] * Task[ i ].memory
							- mu5[ i ] / Task[ i ].process_time
							+ mu6[ i ]
							+ mu7[ t ][ i ][ s ] ) * a[ t ][ i ][ s ];
		ans += mu5[ i ];
	}

#ifdef SUB_SHOW
	cout << ans << ' ';
#endif // SUB_SHOW

	return ans;
}

double sol_sub2()
{
	memset( x, 0, sizeof( x ) );

	for( int t = 0; t < TIME; ++t )
	{
		vector<pair<double,int>> records;

		for( int s = 0; s < SERVER; ++s )
		{
			double coef = Server[ s ].fix_cost
						- mu2[ t ][ s ] * Server[ s ].cpu
						- mu3[ t ][ s ] * Server[ s ].memory
						+ mu4[ t ][ s ]
						- ( t < TIME - 1? mu4[ t + 1 ][ s ] : 0 ); // 時間在最後一個時,沒有 t+1 的時間
			for( int i = 0; i < TASK; ++i )
				coef -= mu1[ t ][ i ][ s ];

			if( coef < 0 )
				x[ t ][ s ] = 1;
			else
			{
				x[ t ][ s ] = 0;
				records.push_back( make_pair( coef, s ) );
			}
		}

		sort( records.begin(), records.end() );
		for( int open = accumulate( x[ t ], x[ t + 1 ], 0 ), i = 0; open < OPEN; ++open, ++i )
			x[ t ][ records[ i ].second ] = 1;
	}

	// Calculate sub-problem result
	double ans = 0;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double coef = Server[ s ].fix_cost
				- mu2[ t ][ s ] * Server[ s ].cpu
				- mu3[ t ][ s ] * Server[ s ].memory
				+ mu4[ t ][ s ]
				- ( t < TIME - 1? mu4[ t + 1 ][ s ] : 0 );
			for( int i = 0; i < TASK; ++i )
				coef -= mu1[ t ][ i ][ s ];
			ans += coef * x[ t ][ s ];
		}

#ifdef SUB_SHOW
	cout << ans << ' ';
#endif // SUB_SHOW

	return ans;
}

double sol_sub3()
{
	memset( y, 0, sizeof( y ) );

	double **coef = new double*[ TIME ];

	for( int i = 0 ; i < TIME; ++i )
		coef[ i ] = new double[ SERVER ];

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			coef[ t ][ s ] = Server[ s ].reopen_cost - mu4[ t ][ s ];

	// Make to satisfy constraint
	for( int s = 0; s < SERVER; ++s )
	{
		double *val = new double[ TIME ], *sol = new double[ TIME ];
		bool *use = new bool[ TIME ];

		for( int t = 0; t < TIME; ++t )
		{
			val[ t ] = coef[ t ][ s ];
			sol[ t ] = 0;
			use[ t ] = false;
		}

		for( int t = 2; t < TIME; ++t )
			if( val[ t ] + sol[ t - 2 ] < sol[ t - 1 ] )
				sol[ t ] = val[ t ] + sol[ t - 2 ], use[ t ] = true;
			else
				sol[ t ] = sol[ t - 1 ];

		for( int t = TIME - 1; t >= 0; )
			if( use[ t ] )
				y[ t ][ s ] = 1, t -= 2;
			else
				--t;

		delete[] val, delete[] sol, delete[] use;
	}

	for( int i = 0; i < TIME; ++i )
		delete[] coef[ i ];
	delete[] coef;

	// Calculate sub-problem result
	double ans = 0;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			ans += ( Server[ s ].reopen_cost - mu4[ t ][ s ] ) * y[ t ][ s ];

#ifdef SUB_SHOW
	cout << ans << ' ';
#endif // SUB_SHOW

	return ans;
}

double sol_sub4()
{
	static int block_qnt = -1; // # of tasks that must be blocked (conservative est.)
	vector<pair<double,int>> coef;

	memset( b, 0, sizeof( b ) );

	if( block_qnt == -1 ) // Find block quantity here
		for( int t = 0; t < TIME; )
		{
			vector<int> exist_task;
			queue<int> bfs;
			set<int> visit;
			int take_by_cpu = 0, take_by_mem = 0, first = TIME, last = 0, cpu_cap = 0, mem_cap = 0;

			// Tasks that may stay in the system now
			for( int i = 0; i < TASK; ++i )
				if( t >= Task[ i ].arrive && t < Task[ i ].deadline )
					bfs.push( i ), visit.insert( i );
			for( ; !bfs.empty(); bfs.pop() )
				for( int i = 0, f = bfs.front(); i < TASK; ++i )
					if( visit.find( i ) == visit.end() && Task[ i ].arrive < Task[ f ].deadline && Task[ i ].deadline > Task[ f ].arrive )
						bfs.push( i ), visit.insert( i );
			exist_task.insert( exist_task.end(), visit.begin(), visit.end() );

			// Time range for possible tasks
			for( int i: exist_task )
				first = min( first, Task[ i ].arrive ), last = max( last, Task[ i ].deadline );
			for( auto& s: Server )
				cpu_cap += s.cpu * ( last - first ), mem_cap += s.memory * ( last - first );

			int &C = take_by_cpu, &M = take_by_mem;
			// Check by CPU
			sort( exist_task.begin(), exist_task.end(), [] ( int a, int b ) { return Task[ a ].cpu < Task[ b ].cpu; } );
			for( int sum = 0; C < (int) exist_task.size() && sum + Task[ exist_task[ C ] ].cpu <= cpu_cap; ++C )
				sum += Task[ exist_task[ C ] ].cpu * Task[ exist_task[ C ] ].process_time;
			// Check by memory
			sort( exist_task.begin(), exist_task.end(), [] ( int a, int b ) { return Task[ a ].memory < Task[ b ].memory; } );
			for( int sum = 0; M < (int) exist_task.size() && sum + Task[ exist_task[ M ] ].memory <= mem_cap; ++M )
				sum += Task[ exist_task[ M ] ].memory * Task[ exist_task[ M ] ].process_time;

			// Update the result
			block_qnt = max( block_qnt, int( visit.size() ) - max( take_by_cpu, take_by_mem ) );
			t = exist_task.empty()? t + 1 : last;
		}

	//block_qnt = accumulate( pb, pb + TASK, 0 );

	for( int i = 0; i < TASK; ++i )
		coef.push_back( make_pair( Task[ i ].penalty - mu5[ i ] + mu6[ i ], i ) ); // (coef,index)
	sort( coef.begin(), coef.end() ); // 係數已照大小排好 (ascending)

	/** TODO: blocked tasks < # of Tasks * block rate constraint may need be modified **/
	for( int idx = 0; idx < TASK && ( idx < block_qnt || coef[ idx ].first < 0 ); ++idx )
		b[ coef[ idx ].second ] = 1;

	// Calculate sub-problem result
	double ans = 0;

	for( int i = 0; i < TASK; ++i )
		ans += ( Task[ i ].penalty - mu5[ i ] + mu6[ i ] ) * b[ i ];

#ifdef SUB_SHOW
	cout << ans << endl;
#endif // SUB_SHOW

	return ans;
}

double sol_sub5()
{
	memset( u, 0, sizeof( u ) );
	for( int i = 0; i < TASK; ++i )
	{
		vector<pair<double,int>> coef;
		for( int s = 0; s < SERVER; ++s )
		{
			double val = 0;
			for( int t = 0; t < TIME; ++t )
				val += OMEGA - mu7[ t ][ i ][ s ];
			coef.push_back( make_pair( val, s ) );
		}
		sort( coef.begin(), coef.end() );
		if( coef[ 0 ].first <= 0 )
			u[ i ][ coef[ 0 ].second ] = 1;
	}

	// Calculate sub-problem result
	double ans = 0;

	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				ans += ( OMEGA - mu7[ t ][ i ][ s ] ) * u[ i ][ s ];
	return ans;
}

void adjust_multiplier()
{
	double denominator = 0, size;

	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;

			// mu1
			for( int i = 0; i < TASK; ++i )
				denominator += pow( a[ t ][ i ][ s ] - x[ t ][ s ], 2 );

			// mu2
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * Task[ i ].cpu;
			tmp -= x[ t ][ s ] * Server[ s ].cpu;
			denominator += pow( tmp, 2 );

			// mu3
			tmp = 0;
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * Task[ i ].memory;
			tmp -= x[ t ][ s ] * Server[ s ].memory;
			denominator += pow( tmp, 2 );

			// mu4
			denominator += pow( x[ t ][ s ] - ( t? x[ t - 1 ][ s ] : 0 ) - y[ t ][ s ], 2 );

			// mu7
			for( int i = 0; i < TASK; ++i )
				denominator += pow( a[ t ][ i ][ s ] - u[ i ][ s ], 2 );
		}
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;

		// mu5
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += a[ t ][ i ][ s ];
		denominator += pow( ( Task[ i ].process_time - tmp ) / Task[ i ].process_time - b[ i ], 2 );

		// mu6 tmp 沿用 (值相同)
		denominator += pow( b[ i ] - Task[ i ].process_time + tmp, 2 );
	}

//	size = lambda * ( z_star - LB ) / denominator; // LB may be zd
	size = lambda * ( z_star - zd ) / denominator;

	// mu1
	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				mu1[ t ][ i ][ s ] = max( 0., mu1[ t ][ i ][ s ] + size * ( a[ t ][ i ][ s ] - x[ t ][ s ] ) );

	// mu2
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * Task[ i ].cpu;
			mu2[ t ][ s ] = max( 0., mu2[ t ][ s ] + size * ( tmp - x[ t ][ s ] * Server[ s ].cpu ) );
		}

	// mu3
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			double tmp = 0;
			for( int i = 0; i < TASK; ++i )
				tmp += a[ t ][ i ][ s ] * Task[ i ].memory;
			mu3[ t ][ s ] = max( 0., mu3[ t ][ s ] + size * ( tmp - x[ t ][ s ] * Server[ s ].memory ) );
		}

	// mu4
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			mu4[ t ][ s ] = max( 0., mu4[ t ][ s ] + size * ( x[ t ][ s ] - ( t? x[ t - 1 ][ s ] : 0 ) - y[ t ][ s ] ) );

	// mu5
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += a[ t ][ i ][ s ];
		mu5[ i ] = max( 0., mu5[ i ] + size * ( ( Task[ i ].process_time - tmp ) / Task[ i ].process_time - b[ i ] ) );
	}

	// mu6
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += a[ t ][ i ][ s ];
		mu6[ i ] = max( 0., mu6[ i ] + size * ( b[ i ] - Task[ i ].process_time + tmp ) );
	}

	// mu7
	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				mu7[ t ][ i ][ s ] = max( 0., mu7[ t ][ i ][ s ] + size * ( a[ t ][ i ][ s ] - u[ i ][ s ] ) );
}

double primal()
{
	double primal_sol = 0;

	memset( pa, 0, sizeof( pa ) );
	memset( px, 0, sizeof( px ) );
	memset( py, 0, sizeof( py ) );
	memset( pb, 0, sizeof( pb ) );
	memset( pu, 0, sizeof( pu ) );
	for( auto& s: Server )
		s.clear();
	for( auto& t: Task )
		t.clear();

	for( int b = 1; b <= BATCH; ++b )
	{
		vector<int> task_list;
		int begin = ( b - 1 ) * TIME / BATCH, end = b * TIME / BATCH;

		// Tasks that exist
		for( int i = 0; i < TASK; ++i )
			if( !Task[ i ].fulfilled() && Task[ i ].arrive < end && Task[ i ].deadline > begin && Task[ i ].deadline - begin - Task[ i ].process_time >= 0 )
				task_list.push_back( i );
		// Which one comes first
		if( PRIMAL_MODE & MU )
			sort( task_list.begin(), task_list.end(), [ b, begin, end ] ( const int& x, const int& y )
					{
						task &tx = Task[ x ], &ty = Task[ y ];
						int buf_x = tx.deadline - begin - tx.process_time + tx.satisfied(), buf_y = ty.deadline - begin - ty.process_time + ty.satisfied();
						if( buf_x < 0 || buf_y < 0 )
							return buf_x < buf_y;
						return Task[ x ].total_value() / Task[ x ].cpu / Task[ x ].memory * Task[ x ].mu_sum( mu1 ) / pow( buf_x, 2 )
						> Task[ y ].total_value() / Task[ y ].cpu / Task[ y ].memory * Task[ y ].mu_sum( mu1 ) / pow( buf_y, 2 );
					}
				);
		// Assign
		for( int i = 0; i < int( task_list.size() ); ++i )
//			if( Task[ task_list[ i ] ].at() == -1 )
		{
			vector<int> server_list;
			for( int i = 0; i < SERVER; ++i )
				server_list.push_back( i );
			if( PRIMAL_MODE & MU )
				sort( server_list.begin(), server_list.end(), [] ( int a, int b ) { return Server[ a ].assoc_rate( mu2, mu3 ) > Server[ b ].assoc_rate( mu2, mu3 ); } );
			for( int s = 0; s < SERVER; ++s )
				if( Server[ server_list[ s ] ].take_primal( Task[ task_list[ i ] ] ) )
					break;
		}
//			else
//				Server[ Task[ task_list[ i ] ].at() ].take_primal( Task[ task_list[ i ] ] );
	}

	// 關掉入不敷出的機器
	if( PRIMAL_MODE & TURN_OFF )
	{
		vector<pair<int,int>> val;
		for( int s = 0; s < SERVER; ++s )
			if( Server[ s ].is_on() )
				val.push_back( make_pair( Server[ s ].total_value(), s ) );
		sort( val.begin(), val.end() );
		// 不要關太多使得開機的數量少於限制
		for( int s = 0; int( val.size() ) - s > OPEN && val[ s ].first < 0; ++s )
			Server[ val[ s ].second ].drop_all();
	}

	// Adjust px
	memset( px, 0, sizeof( px ) );
	for( int t = 0; t < TIME; ++t )
		for( int i = 0; i < TASK; ++i )
			for( int s = 0; s < SERVER; ++s )
				if( pa[ t ][ i ][ s ] > px[ t ][ s ] )
					px[ t ][ s ] = pa[ t ][ i ][ s ];
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; accumulate( px[ t ], px[ t + 1 ], 0 ) < OPEN; ++s )
			px[ t ][ s ] = 1;

	// Adjust py
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
			if( px[ t ][ s ] && ( !t || !px[ t - 1 ][ s ] ) && any_of( px, px + t, [ s ] ( double *n ) { return n[ s ] == 1; } ) )
				py[ t ][ s ] = 1;
			else
				py[ t ][ s ] = 0;

	// Adjust pb
	for( int i = 0; i < TASK; ++i )
	{
		double tmp = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				tmp += pa[ t ][ i ][ s ];
		pb[ i ] = tmp != Task[ i ].process_time; // 不相等就設 1
	}

	// Adjust pu
	for( int i = 0; i < TASK; ++i )
		for( int s = 0; s < SERVER; ++s )
			for( int t = 0; t < TIME; ++t )
				if( pa[ t ][ i ][ s ] == 1 )
				{
					pu[ i ][ s ] = 1;
					break;
				}

	// calculate upper bound
	for( int t = 0; t < TIME; ++t )
		for( int s = 0; s < SERVER; ++s )
		{
			primal_sol += Server[ s ].fix_cost * px[ t ][ s ] + Server[ s ].reopen_cost * py[ t ][ s ];
			for( int i = 0; i < TASK; ++i )
				primal_sol += -Task[ i ].value[ t ] * pa[ t ][ i ][ s ];
		}
	for( int i = 0; i < TASK; ++i )
	{
		primal_sol += Task[ i ].penalty * pb[ i ];
		for( int s = 0; s < SERVER; ++s )
			primal_sol += -OMEGA * pu[ i ][ s ];
	}

	return primal_sol;
}

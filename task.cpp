struct task
{
	double pcpu, pmemory;
	static int waiting_time, ID;
	static bool burst[ MAX_TIME ];
	static vector<int> burst_time, burst_period;
	const static int BURST_GRP = 5;//, BURST_PART = 4;
	int arrive, delta[ MAX_TIME ], value[ MAX_TIME ], cpu, memory;
	int process_time, penalty, id, deadline, served[ MAX_TIME ];
	/* waiting_time = epsilon, process_time = gamma */
	static int MAX_V, BASE_GAMMA, PENALTY, BASE_CPU, BASE_MEMORY;

	void init()
	{
		id = ID++;
		value[ 0 ] = my_rand() % MAX_V + 1;
		for( int i = 1; i < TIME; ++i )
			value[ i ] = value[ 0 ];
		pcpu = cpu = my_rand() % BASE_CPU + 1;
		pmemory = memory = my_rand() % BASE_MEMORY + 1;

		process_time = my_rand() % BASE_GAMMA + 1;
#ifndef BURST_TASK
		int come = my_rand() % ( TIME - process_time + 1 );
#else


		int rand_grp = my_rand() % BURST_GRP, come = burst_time[ rand_grp ] + rand() % burst_period[ rand_grp ];
		if( come + process_time > TIME )
			come = TIME - process_time;
#endif
		deadline = min( come + process_time + /* my_rand() % */ waiting_time, TIME ); // 此刻「前」處理完
		arrive = come;
		delta[ come ] = 1;
		penalty = PENALTY;
		fill( served, served + MAX_TIME, -1 );
	}

	static void init_re()
	{
		fill( burst, burst + BURST_GRP, true );
		random_shuffle( burst, burst + TIME );

		for( int i = 0; i < TIME; ++i )
			if( burst[ i ] )
				burst_time.push_back( i );
		while( burst_period.size() < burst_time.size() )
			burst_period.push_back( rand() % 5 + 1 );
	}

	int load() { return cpu + memory; }

	void show()
	{
		printf( "%d\t%d\t%d\t%d\t%d\n", value[ 0 ], cpu, memory, arrive, process_time );
	}

	int total_value() const
	{
		vector<int> val( value, value + TIME );
		sort( val.begin(), val.end() );
		return accumulate( val.begin(), val.begin() + process_time, 0 );
	}

	double mu_sum( double mu1[ MAX_TIME ][ MAX_TASK ][ MAX_SERVER ] )
	{
		double sum = 0;
		for( int t = 0; t < TIME; ++t )
			for( int s = 0; s < SERVER; ++s )
				sum += mu1[ t ][ id ][ s ];
		return sum;
	}

	int at()
	{
		auto it = find_if( served, served + MAX_TIME, [] ( int x ) { return ~x; /* x != -1 */ } );
		return it == served + MAX_TIME? -1 : *it;
	}

	bool fulfilled()
	{
		return satisfied() >= process_time;
	}

	void clear()
	{
		fill( served, served + MAX_TIME, -1 );
	}

	int satisfied()
	{
		return count_if( served, served + MAX_TIME, [] ( int x ) { return ~x; /* x != -1 */ } );
	}
};

int task::ID = 0, task::waiting_time = 20, task::MAX_V = 50, task::BASE_GAMMA = TIME / 5, task::PENALTY = 40, task::BASE_CPU = 50, task::BASE_MEMORY = 50;
bool task::burst[ MAX_TIME ];
vector<int> task::burst_time, task::burst_period;
